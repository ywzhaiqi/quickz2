/* 
说明：
很多不方便或者不能在菜单里实现的功能，可以在本文件内实现，如添加很多全局热键，下面是示例：
^!+a::			自定义热键
^!+w::			调用已有菜单项
^!+m::			不通过QZ直接调用剪贴板信息或窗体信息
 */
 
 /*
全局设置>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*/
CustomInit() {
	global ScitePath:="C:\Program Files\AutoHotkey\SciTE\SciTE.exe"
}
/*
我定义的快捷键>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*/
;=======================================================
;【xxxxxx】
^!+a::
	sendinput,11111111
 return
 
^!+w::		;菜单项：保存文本到【D:\TempSave】文件夹
	Global gMenuZ
    gMenuZ.Data := QZ_GetData()         ;初始化
    QZ_GetWinInfo(gMenuZ.Data)    ;获取当前窗口信息
    QZ_GetClip(gMenuZ.Data)     ;获取剪切板数据，并对文件类型进行预处理
	;gosub,B8CC07F0-5809-40BC-81EC-6B24FE583185	;本字符串为【标识ID】,可从【浏览】-【选择菜单项】窗体下，按住Ctrl后点击菜单名称后即复制【标识ID】到剪贴板
 return
 
^!+m::
	FilesArray:={}
	QZ_GetClip(FilesArray)	;获取剪切板数据
	msgbox % FilesArray.files
	msgbox % FilesArray.text

	ExeArray:={}
	QZ_GetWinInfo(ExeArray)	;获取当前窗口信息
	msgbox % ExeArray.x
	msgbox % ExeArray.y
	msgbox % ExeArray.HWND
	msgbox % ExeArray.WinClass
	msgbox % ExeArray.WinExe
	msgbox % ExeArray.WinControl
	msgbox % ExeArray.WinTitle
 return
 
 /*
我定义程序操作>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*/
;=======================================================
;【Total Command相关操作】
;【激活Total Command】
;~ #e::
	;~ TLAppPath:=QZData("%TOTALCMD%")
	;~ ;TLAppPath:="d:\KawvinApps\totalcmd\TOTALCMD.EXE"
	;~ downloads:=""
	;~ BT:=""
	;~ DetectHiddenWindows, on
	;~ IfWinNotExist	ahk_class TTOTAL_CMD
		;~ Run,%TLAppPath%,,Max
	;~ IfWinNotActive ahk_class TTOTAL_CMD
	;~ {
		;~ if (WinActive("ahk_class QQBrowser_WidgetWin_1") or WinActive("ahk_class XLUEFrameHostWnd"))	;QQ浏览器
		;~ { 
			;~ Run,%TLAppPath% /T /R=%downloads%
			;~ Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
            ;~ WinWait, ahk_class TTOTAL_CMD 
            ;~ WinActivate 
		;~ } 
		;~ else if WinActive("ahk_class MozillaWindowClass")  	;FireFox浏览器
		;~ { 
			;~ Run,%TLAppPath% /T /R=%BT%
			;~ Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
            ;~ WinWait, ahk_class TTOTAL_CMD 
            ;~ WinActivate 
		;~ } 
		;else if WinActive("ahk_class OpusApp") or WinActive("ahk_class XLMAIN") 
		{ 
            ;Run %TLAppPath% /T /R %Yxwork% 
;WinWait, ahk_class TTOTAL_CMD 
            ;WinActivate 
		} 
		;~ else 
		;~ { 
			;~ Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
			;~ WinWait,ahk_class TTOTAL_CMD
			;~ WinActivate
		;~ }
	;~ } else {
		;~ Postmessage, 1075, 2000, 0,, ahk_class TTOTAL_CMD		;最小化
	;~ }
;~ Return

;~ ;【实现右键双击返回上级目录功能】
;~ ~RButton::
	;~ if (A_PriorHotkey=A_ThisHotkey and A_TimeSincePriorHotkey<300)
	;~ {
		;~ IfWinActive ahk_class TTOTAL_CMD
			;~ Postmessage, 1075, 2002, 0,, ahk_class TTOTAL_CMD	;返回上级目录
	;~ }
;~ return

;~ ;【实现先按下左键，后按下右键，关闭标签】
;~ ~LButton & RButton::
	;~ IfWinActive ahk_class TTOTAL_CMD
		;~ Postmessage, 1075, 3007, 0,, ahk_class TTOTAL_CMD 	;关闭标签
;~ return


/*
我的快捷替换短语>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*/
; 【*】 的作用是不用输入终止符即自动替换
;=======================================================
;【ad*----地址类】
:*:]adthl::        ;联发地址-田hl
	SendInput   {raw}xxxxxxx，田hl，186-xxxx-xxxx
return
;=======================================================
;【em*----邮箱类】
:*:]emqq::        ;QQ邮箱
	SendInput   {raw}28xxxx@qq.com
return
;=======================================================
;【tel*----电话类】
:*:]telhwz::        ;电话-韩wz
	SendInput   {raw}韩wz，185-xxxx-xxxx
return
;=======================================================
;【-*----日期类】
:*:]-=::       ;今天，20160602
	SendInput  %A_YYYY%%A_MM%%A_DD%
return
:*:]--::       ;今天，2016-06-22
	SendInput  %A_YYYY%-%A_MM%-%A_DD%
return
:*:]-0::       ;今天，2016.06.22
	SendInput  %A_YYYY%.%A_MM%.%A_DD%
return
;=======================================================
;【**----名称类】
:*:]lf::       ;lf集团有限公司
	SendInput  lf集团有限公司
return