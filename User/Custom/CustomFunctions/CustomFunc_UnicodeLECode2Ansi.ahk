﻿CustomFunc_UnicodeLECode2Ansi(text)
{
	text := "0x" . text
	VarSetCapacity(LE, 2, 0)
	NumPut(text, LE)
	VarSetCapacity( Ansi, 2, 0)
	VarSetCapacity( 中间变量, 2, 0)
	DllCall( "MultiByteToWideChar", Int, 936, Int, 0, UInt, &LE, Int, -1, Str, 中间变量, Int, 2)
	DllCall( "WideCharToMultiByte", Int, 936, Int, 0, UInt, &中间变量, Int, -1, Str, Ansi, Int, 2, Int, 0,Int, 0)
	Return Ansi
}