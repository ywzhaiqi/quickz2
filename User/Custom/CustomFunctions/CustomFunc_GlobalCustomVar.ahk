﻿/*
API=GlobalCustomVar
Tips=自定义全局变量赋值
Author=LL
Version=0.2
*/

/*
    函数: GlobalCustomVar
        定义一个全局对象，用户自定义的变量经过计算后存放到此对象中

    参数:
        无需参数

    返回:
        必然返回一个全局的 GlobalCustomVar 对象，同时以赋值方式返回到用户指定的对象。

    作者: yyy

    版本: 0.2
*/

GlobalCustomVar() {
	global GlobalCustomVar
	GlobalCustomVar:={}
	GlobalCustomVar.OSBit:=A_Is64bitOS?64:32
	GlobalCustomVar.OSBitOmit32:=A_Is64bitOS?64:""
	return GlobalCustomVar
}