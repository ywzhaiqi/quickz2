﻿CustomFunc_MenuZAll()
{
    global PUMCount:=0                ;Kawvin修改
    global PUMFirst:={}		 ;Kawvin修改
    Global gQZConfig, gMenuZ, objData
    gMenuZ.FilterResult := {} ; 重新初始化筛选器对象
    gMenuZ.Data := QZ_GetData()         ;调用Ahk文件：QZ_API.ahk，作用：初始化
    QZ_GetKeyword(A_ThisHotkey, gMenuZ.Data)     ;调用Ahk文件：QZ_API.ahk，作用：
    QZ_GetWinInfo(gMenuZ.Data)    ;调用Ahk文件：QZ_API.ahk，作用：获取当前窗口信息
    CustomFunc_QZ_GetClip(gMenuZ.Data)     ;调用Ahk文件：QZ_API.ahk，作用：获取剪切板数据，并对文件类型进行预处理
    objData := gMenuZ.Data
    ;=======================================
    PumMenu := gMenuZ.PUM.CreateMenu(gMenuZ.PUMParam)    ;调用Ahk文件：PUM.ahk，作用：创建菜单
    MenuZ_AddItem(PumMenu, gQZConfig.MenuZ,objData)         ;Kawvin修改    ;调用Ahk文件：本文件，作用：添加菜单项
    ;~ RunItem:=PumMenu.items[1]
    if (objData.SepMode=1) and (PUMCount=1)
    {
        ;~ RunItem:=PumMenu.items[1]
        MenuZ_Do("OnRun",PUMFirst)
    } else
        PumMenu.Show(gMenuZ.Data.x, gMenuZ.Data.y)
}