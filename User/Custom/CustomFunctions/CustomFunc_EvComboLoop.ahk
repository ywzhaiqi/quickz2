﻿;Everything 快捷操作函数 by hyknife
CustomFunc_EvComboLoop(num_X:=0, num_Y:=2, num_Z:=1)		;[Everything 筛选循环]
{
    SendMessage, 0x147, 0, 0, ComboBox1, A
    ChoicePos = %ErrorLevel%
    ChoicePos += 1

    If (num_X = 0)
    {
        ChoicePos += 1
        If (ChoicePos = 9)
            ChoicePos = 1
    }
    Else If (ChoicePos = num_X)
    {
        ChoicePos := num_Y
    }
    Else If (ChoicePos = num_Y)
    {
        ChoicePos := num_Z
    }
    Else If (ChoicePos = num_Z)
    {
        ChoicePos := num_X
    }
    Else
    {
        ChoicePos := num_X
    }

    Control, Choose, %ChoicePos%, ComboBox1, A
    Return
}