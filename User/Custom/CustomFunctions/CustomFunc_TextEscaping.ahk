﻿/*
Plugin=转换为AHK原义文本
Name1=转换为AHK原义文本（单行）
Command1=CustomFunc_TextEscaping2SingleLine
Name2=转换为AHK原义文本（延续片段）
Command2=CustomFunc_TextEscaping2ContinuationSection
Author=LL
Version=0.2
*/

;此函数对获得的文本进行转义，具有特殊作用的符号将被转换为原义的字符。
CustomFunc_TextEscaping2SingleLine(Text)
{
    If Text=
    {
        QZSel:=CustomFunc_QZSel()
        Text:=QZSel.text
        if Text=
            msgbox 无法获取文本
    }
    arrChar:=["``",",","%",";"]
    for k,v in arrChar
    {
        ;~ msgbox % v
        Text:=RegExReplace(Text, v, "``" v, OutputVarCount)
    }
    StringReplace, Text, Text, `n, ``n, All
    StringReplace, Text, Text, `r, ``r, All
    StringReplace, Text, Text, `b, ``b, All
    StringReplace, Text, Text, `t, ``t, All
    StringReplace, Text, Text, `v, ``v, All
    StringReplace, Text, Text, `a, ``a, All
    StringReplace, Text, Text, `f, ``f, All
    return Text
}

CustomFunc_TextEscaping2ContinuationSection(Text)
{
    If Text=
    {
        QZSel:=CustomFunc_QZSel()
        Text:=QZSel.text
        if Text=
            msgbox 无法获取文本
    }
    arrChar:=["``","%"]
    for k,v in arrChar
    {
        ;~ msgbox % v
        Text:=RegExReplace(Text, v, "``" v, OutputVarCount)
    }
    Text=tEscaped=`n(`n%Text%`n)%A_Space%;延续片段已存到变量 tEscaped 中
    return Text
}