﻿/*
Filter=CustomFilter_is2Files
Tips=此函数当所选为2个文档时返回true
Author=Kawvin
Version=0.1
*/

CustomFilter_is2Files() { 	
	Global gMenuZ
	FileList=% gMenuZ.Data.files
	if (FileList="")
		Return false ;kawvin修改
	;~ IfInString,FileList,`n
	OnlyFile:=true ;先假定不包含文件
	{
		Loop, Parse, FileList, `n, `r
		{
			FileAttrib:=FileExist(A_LoopField)
			IfInString,FileAttrib,D
			{
				OnlyFile:=false
				break
			}
		}
	}
	if (OnlyFile=False)
		return False

	FileCount:=0 
	{
		Loop, Parse, FileList, `n, `r
			FileCount:=A_Index
	}
	if (FIleCount=2)
		return true
	else
		return False
}