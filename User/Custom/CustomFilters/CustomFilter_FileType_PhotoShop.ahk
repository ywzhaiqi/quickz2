﻿/*
Filter=CustomFilter_FileType_PhotoShop
Tips=此函数当所选仅包含 PhotoShop 支持的文件时返回 true
Author=LL
Version=0.1
*/

CustomFilter_FileType_PhotoShop(FileList="") { 	
    ExtNameSupported=eps;fl3;kmz;iff;tdi;jpg;jpeg;jpe;jpf;jpx;jp2;j2c;j2k;jpc;jps;exr;pcx;pdf;pdp;raw;psdx;pct;pict;pxr;png;pns;pbm;pgm;ppm;pnm;pfm;ram;hdr;rgbe;xyze;sct;stl;tga;vda;icb;vst;tif;tiff;obj;mpo;ai3;ai4;ai5;ai6;ai7;ai8;ps;ai;epsf;epsp;wbm;wbmp;psd
	;枚举所有支持的文件类型，正常状态下只需要修改筛选函数名以及支持的文件后缀即可（第2行、第8行、第10行）
	if not FileList
	{
		Global gMenuZ
		FileList=% gMenuZ.Data.files
	}
	if (FileList="")
		Return false ;kawvin修改
	Result:=true ;先假定为真
	;~ IfInString,FileList,`n
	{
		Loop, Parse, FileList, `n, `r
		{
			FileAttrib:=FileExist(A_LoopField)
			IfInString,FileAttrib,D
			{
				Result:=false
				break
			}
            SplitPath, A_LoopField, OutFileName, OutDir, OutExtension, OutNameNoExt, OutDrive
            IfNotInString, ExtNameSupported, %OutExtension%
            {
				Result:=false
				break
            }
		}
	}
	Return Result
}