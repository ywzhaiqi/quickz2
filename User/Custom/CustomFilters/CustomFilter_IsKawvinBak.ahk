﻿/*
Filter=CustomFilter_IsKawvinBak
Tips=Kawvin编程常用的备份文件后缀名支持的文件时返回true
Author=Kawvin
Version=0.1
*/

CustomFilter_IsKawvinBak() { 	
    BakString=例;bak;1;2;3;4;新;原;旧;老
	;枚举所有支持的文件类型，正常状态下只需要修改筛选函数名以及支持的文件后缀即可（第2行、第8行、第10行）
	Global gMenuZ
	FileList=% gMenuZ.Data.files
	if (FileList="")
		Return false ;kawvin修改
	Result:=false ;先假定为真
	;~ IfInString,FileList,`n
	{
		Loop, Parse, FileList, `n, `r
		{
			FileAttrib:=FileExist(A_LoopField)
			IfInString,FileAttrib,D
			{
				Result:=false
				break
			}
            SplitPath, A_LoopField, OutFileName, OutDir, OutExtension, OutNameNoExt, OutDrive
			loop,Parse,BakString,`;
			{
				
				TemStr:=A_LoopField
				IfInString,OutExtension , %TemStr%
				{
					Result:=true
					break
				}
			}
		}
	}
	Return Result
}