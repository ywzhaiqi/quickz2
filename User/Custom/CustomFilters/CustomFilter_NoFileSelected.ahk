﻿/*
Filter=CustomFilter_NoFileSelected
Tips=此函数当未选中任何文件或文件夹时返回 true
Author=LL
Version=0.1
*/

CustomFilter_NoFileSelected() {  ;此函数当未选中任何文件或文件夹时返回 true
	if not FileList
	{
		Global gMenuZ
		FileList=% gMenuZ.Data.files
	}
	If % StrLen(FileList)
		NoFileSelected:=false
	else
		NoFileSelected:=true
	Return NoFileSelected
}