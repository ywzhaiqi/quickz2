﻿/*
Plugin=CustomFunc_Text2QRCode
Name1=文本生成二维码
Command1=CustomFunc_Text2QRCode
Author=Array
Version=0.1
*/

CustomFunc_Text2QRCode()
{
    global f
    GUI,QRCode:Destroy
    GUI,QRCode:Add,Pic,x20 y20 w500 h-1 hwndhimage,% f:=CustomFunc_GEN_QR_CODE(Substr(QZData("Text"),1,850))
    GUI,QRCode:Add,Text,x20 y542 h24,按Esc关闭                                   点击图片可显示源文本
    GUI,QRCode:Add,Button,x420 y540 w100 h24 gQZ_QRCode_SaveAs,另存为(&S)
    GUI,QRCode:Show,w540 h580, 文本生成二维码
    ;======函数中的以下部分提供点击图片提示源文本的功能======
    QZSel:=CustomFunc_QZSel()
    SourceText:=CustomFunc_TextEscaping2SingleLine(QZSel.text)
    ScriptContent=
    (
    ;Menu,Tray,Icon, %A_ScriptDir%\User\Custom\CustomIcons\CustomIcon_Text_QRCode.ico ;图标放在托盘颜色太淡，以后再换一个
    Menu, Tray, Tip, 文本生成二维码
    #ifWinActive,文本生成二维码
    ~Esc::goto AutoExitApp
    ~LButton::`nMouseGetPos, OutputVarX, OutputVarY, OutputVarWin, OutputVarControl
    if `% OutputVarControl="Static1"
    {
        ToolTip,%SourceText%
        SetTimer, RemoveQRCodeTextTip, 2000
    }
    else
        goto AutoExitApp
    return
    AutoExitApp:
    sleep 500
    IfWinNotExist 文本生成二维码
        ExitApp
    return
    #ifWinNotExist,文本生成二维码
    ~LButton::ExitApp
    RemoveQRCodeTextTip:
    SetTimer, RemoveQRCodeTextTip, Off
    ToolTip
    return
    )
    CustomFunc_PipeRun(ScriptContent)
}

QRCodeGUIEscape:
  GUI,QRCode:Destroy
  FileDelete, % f
return

QZ_QRCode_SaveAs:
  Fileselectfile,nf,s16,,另存为,PNG图片(*.png)
  If not strlen(nf)
    return
  nf := RegExMatch(nf,"i)\.png") ? nf : nf ".png"
  FileCopy,%f%,%nf%,1
return

CustomFunc_GEN_QR_CODE(string,file="")
{
  sFile := strlen(file) ? file : A_Temp "\" A_NowUTC ".png"
  SplitPath, A_LineFile, , QRCode_Path
  DllCall( QRCode_Path "\quricol32.dll\GeneratePNG","str", sFile , "str", string, "int", 4, "int", 2, "int", 0)
  Return sFile
}