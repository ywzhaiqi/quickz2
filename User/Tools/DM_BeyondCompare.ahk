﻿/*
Plugin=BeyondCompare
Name1=BeyondCompare 比较（动态菜单）
Command1=DM_BCompare
Author=LL，Kawvin修改
Version=0.2
*/

DM_BCompare(aParam) ; 此Plugin执行的command为Test，代表执行Test函数,函数请预留一个aParam，用于QuickZ传递参数过来。
{
    icon:=A_ScriptDir . "\User\Icons\BCompare.ico"
    ;~ BCompareMenu := MenuZ_GetSub() ; 获取一个子菜单对象
    BCompareMenu := MenuZ_GetSibling() ; 获取一个同级菜单对象
    BCompareMenu.Add({name:"&B  [BC比较] 左键添加左侧 右键添加右侧并比较 中键上次比较",icon: icon,uid:{Handle:"DM_BComparing_Handle",Data:aParam}}) ; Handle的值为点击菜单后执行的功能
    return BCompareMenu  ; 必须返回子菜单对象
}


DM_BComparing_Handle(aMsg, aObj) 
{
    ;Global gMenuZ
    FileFullPathList:=QZData("Files")
    CompArray:=[]
    loop,Parse,FileFullPathList,`n,`r
    {
        if(A_index=1)
            CompArray[1]:=A_LoopField
        if(A_index=2)
        {
            CompArray[2]:=A_LoopField
            break
        }
        CompArray[2]:=""
    }
    If (aMsg = "OnRun")
    {
        MyFun_BCompareLeft_Handle(CompArray)
    }
	else if(aMsg = "OnRButton")	
    {
		MyFun_BCompareRight_Handle(CompArray)
        ;gMenuZ.PUM.Destroy()
        ;winclose,A
    }
    else If (aMsg = "onmbutton")
    {
		MyFun_BCompareMiddle_Handle()
        ;gMenuZ.PUM.Destroy()
        ;winclose,A
    }
}

MyFun_BCompareLeft_Handle(CompArray) 
{
    BCAppPath:=QZData("%BCompare%")
    if(CompArray[2]!="")
        Run,% BCAppPath " """ CompArray[1] """ """ CompArray[2] """"
    else
    {
        SplitPath, A_LineFile, OutFileName, BCompareIniDir, OutExtension, BCompareIniFileNameNoExt, OutDrive
        SelectedFile:=CompArray[1]
        IniWrite, %SelectedFile%, %BCompareIniDir%\%BCompareIniFileNameNoExt%.ini, BeyondCompareMain, LeftFile
        SelectedFile:=""
        IniWrite, %SelectedFile%, %BCompareIniDir%\%BCompareIniFileNameNoExt%.ini, BeyondCompareMain, RightFile
    }
}
MyFun_BCompareMiddle_Handle()
{
    BCAppPath:=QZData("%BCompare%")
    SplitPath, A_LineFile, OutFileName, BCompareIniDir, OutExtension, BCompareIniFileNameNoExt, OutDrive
    IniRead, BCompareLeftFile, %BCompareIniDir%\%BCompareIniFileNameNoExt%.ini, BeyondCompareMain, LeftFile, %A_Space%
    IniRead, BCompareRightFile, %BCompareIniDir%\%BCompareIniFileNameNoExt%.ini, BeyondCompareMain, RightFile, %A_Space%
    if(BCompareLeftFile!="")&&(BCompareRightFile!="")
        Run,%  BCAppPath " """ BCompareLeftFile """ """ BCompareRightFile """"
    else
        return
    sleep,200
    send,{esc}
}

MyFun_BCompareRight_Handle(CompArray) 
{
    BCAppPath:=QZData("%BCompare%")
    if(CompArray[2]!="")
        Run,%  BCAppPath " """ CompArray[1] """ """ CompArray[2] """"
    else
    {
        SplitPath, A_LineFile, OutFileName, BCompareIniDir, OutExtension, BCompareIniFileNameNoExt, OutDrive
        IniRead, BCompareLeftFile, %BCompareIniDir%\%BCompareIniFileNameNoExt%.ini, BeyondCompareMain, LeftFile, %A_Space%
        SelectedFile:=CompArray[1]
        IniWrite, %SelectedFile%, %BCompareIniDir%\%BCompareIniFileNameNoExt%.ini, BeyondCompareMain, RightFile
        Run,%  BCAppPath " """ BCompareLeftFile """ """ SelectedFile """"
    }
    sleep,200
    send,{esc}
}