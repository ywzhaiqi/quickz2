﻿/*
Plugin=DM_Creat_files
Name1=从模板文件夹动态生成新建文档菜单
Command1=DM_Creat_files
Author=Kawvin
Version=0.1版本
*/

DM_Creat_files(aParam){    ; 此Plugin执行的command为Test，代表执行Test函数,函数请预留一个aParam，用于QuickZ传递参数过来。
	;icon:=A_ScriptDir . "\User\Icons\add.ico"
    DM_Creat_files := MenuZ_GetSibling() ; 获取一个菜单对象，不是子菜单，是同级菜单
	DM_Creat_files.SetParams({iconsSize:24})
	;DM_Creat_files := MenuZ_GetSub() ; 获取一个子菜单对象
	MyFileArray:={"doc":"Word 97-2003文档","docx":"Word 文档","ahk":"AutoHotkey脚本","ppt":"PowerPoint 93-2003演示文稿","pptx":"PowerPoint 演示文稿","rar":"WinRAR 压缩文件","txt":"文本文件","xls":"Excel 97-2003工作簿","xlsm":"Excel 启用宏的工作簿","xlsx":"Excel 工作簿","zip":"WinRAR Zip压缩文件","dwg":"AutoCAD 图形","psd":"Adobe Photoshop 图像","pdf":"PDF 文档"}
	IndexArray:=["1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	SplitPath,A_LineFile,,MyOutDir
	FileList:=""
	Loop, Files, %MyOutDir%\*.*
		FileList = %FileList%%A_LoopFileFullPath%`n
	MenuList:=""
	Loop, Parse, FileList, `n			;读取脚本所在目录下的所有文件
	{
		if A_LoopField =  ; 忽略列表末尾的最后一个换行符 (空项).
			continue
		SplitPath,A_LoopField,,,MyOutExt,MyOutNameNoExt
		if (MyOutNameNoExt="1")	;如果是以1命名的模板文件
		{
			if(MyFileArray[MyOutExt]!="")
				name:=MyFileArray[MyOutExt]
			else
				name:=MyOutExt . " 文档"
			SouFile:=A_LoopField
			MenuList=%MenuList%%name%`|%SouFile%`|%MyOutExt%`n
		}
	}
	Sort, MenuList ,CL	;按文档名称进行排序
	icon=%A_WinDir%\System32\shell32.dll`:4
	DM_Creat_files.Add({name:"&F  文件夹",icon: icon,uid:{Handle:"DM_Creat_files_Handle",Action:"CreatFolder",Data:""}}) ; Handle的值为点击菜单后执行的功能
	DM_Creat_files.Add() 
	Loop, Parse, % MenuList, `n		;生成子菜单
	{
		if A_LoopField =  ; 忽略列表末尾的最后一个换行符 (空项).
			continue
		StringSplit, MenuItemArray, A_LoopField ,|
		name:="&" . IndexArray[A_index] . "  " . MenuItemArray1
		SouFile:=MenuItemArray2
		;~ icon:=MyOutDir . "\文档图标\" . MenuItemArray3 . ".ico"
		Icon := Select_Value_GetIcon(SouFile, MenuItemArray3)
		DM_Creat_files.Add({name:name,icon: Icon,uid:{Handle:"DM_Creat_files_Handle",Action:"CreatFile",Data:SouFile}}) ; Handle的值为点击菜单后执行的功能
	}
	DM_Creat_files.Add() 
	icon=%A_ScriptDir%\User\Icons\default.icl`:53
	DM_Creat_files.Add({name:"&Z  添加到文件模板",icon: icon,uid:{Handle:"DM_Creat_files_Handle",Action:"AddModel",Data:""}}) ; Handle的值为点击菜单后执行的功能
	;~ icon=%A_ScriptDir%\User\Icons\default.icl`:54
	;~ DM_Creat_files.Add({name:"&X  删除选中文件模板",icon: icon,uid:{Handle:"DM_Creat_files_Handle",Action:"DelModel",Data:""}}) ; Handle的值为点击菜单后执行的功能
    return DM_Creat_files  ; 必须返回子菜单对象
}

DM_Creat_files_Handle(aMsg, aObj)
{
    If (aMsg = "OnRun")
		DM_Creat_Flies_Action(aObj.Uid.Action,aObj.Uid.Data)			;左键【新文件夹/文件/添加模板】
	else If (aMsg = "OnRButton")
		DM_Delete_Modes_Action(aObj.Uid.Action,aObj.Uid.Data)	;右键【删除模板】
	;~ else If (aMsg = "onmbutton")
}
DM_Delete_Modes_Action(Action,SouFile)		;删除模板文件
{
	if (Action="CreatFile")		;删除模板文件
	{
		SplitPath,SouFile,,,MyOutExt
		FileCopy, %SouFile%, %DesFile%
		MsgBox, 36, 提示, 确认删除以下模板文件？`n`n【模板名称】：%SouFile%`n【模板后缀】：%MyOutExt%
		IfMsgBox,Yes
			filedelete,%SouFile%
		return
	}
	sleep,200
	send,{Esc}{esc}{esc}
}
DM_Creat_Flies_Action(Action,SouFile)
{
	if (Action="AddModel")		;添加模板文件
	{
		MySelModel:=QZData("files")
		if !strlen(MySelModel)
		{
			MsgBox, 64, 提示, 未选择文件！
			return
		}
		ifinstring,MySelModel,"`n"
		{
			MsgBox, 64, 提示, 请选择单个文件！
			return
		}
		SplitPath,MySelModel,,,MyOutExt
		SplitPath,A_LineFile,,MyOutDir
		if RegExMatch(MyOutDir,":\\$")		;如果是盘符
			DesFile=%MyOutDir%1.%MyOutExt%
		else
			DesFile=%MyOutDir%\1.%MyOutExt%
		FileCopy, %MySelModel%, %DesFile%
		return
	}
	
	CurWinClass:=QZData("winclass") ;将获取的class名赋值给用户变量
	Curhwnd:=QZData("hWnd")
	if (CurWinClass="ExploreWClass") or (CurWinClass="CabinetWClass") ;如果当前激活窗口为资源管理器
	{
		DirectionDir:=Explorer_GetPath_Kawvin(Curhwnd)
		IfInString,DirectionDir,`;		;我的电脑、回收站、控制面板等退出
			return
	}
	if (CurWinClass="WorkerW") or (CurWinClass="Progman") ;如果当前激活窗口为桌面
	{
		;DirectionDir=%A_Desktop%
		DirectionDir:=Explorer_GetPath_Kawvin(Curhwnd)
	}
	if (CurWinClass="Shell_TrayWnd") ;如果当前激活窗口为任务栏
		return
	if (CurWinClass="TTOTAL_CMD") ;如果当前激活窗口为TC
	{
		IfWinNotActive ahk_class TTOTAL_CMD
		{
			Postmessage, 1075, 2015, 0,, ahk_class TTOTAL_CMD	;最大化
			WinWait,ahk_class TTOTAL_CMD
			WinActivate
		}
		Postmessage, 1075, 332, 0,, ahk_class TTOTAL_CMD	;光标定位到焦点地址栏
		sleep 300
		PreClipboard:=Clipboard
		PostMessage,1075,2029,0,,ahk_class TTOTAL_CMD ;获取路径
		sleep 100
		DirectionDir:=Clipboard
		Clipboard:=PreClipboard
	}
	If(DirectionDir="ERROR")		;错误则退出
		return
	;新建文件名
	InputBox, NewFileName, 输入, 请输入文件名称, , 360, 130
	if ErrorLevel
		return
	if (NewFileName="")
		return
	if (Action="CreatFile")
	{
		SplitPath,SouFile,,,MyOutExt
		if RegExMatch(DirectionDir,":\\$")		;如果是盘符
			DesFile=%DirectionDir%%NewFileName%.%MyOutExt%
		else
			DesFile=%DirectionDir%\%NewFileName%.%MyOutExt%
		FileCopy, %SouFile%, %DesFile%
	} 
	if (Action="CreatFolder")
	{
		if RegExMatch(DirectionDir,":\\$")		;如果是盘符
			DesFile=%DirectionDir%%NewFileName%
		else
			DesFile=%DirectionDir%\%NewFileName%
		FileCreateDir, %DesFile%
	}
}

Explorer_GetPath_Kawvin(hwnd="")
{
	if !(window := Explorer_GetWindow_Kawvin(hwnd))
		return ErrorLevel := "ERROR"
	if (window="desktop")
		return A_Desktop
	path := window.LocationURL
	path := RegExReplace(path, "ftp://.*@","ftp://")
	StringReplace, path, path, file:///
	StringReplace, path, path, /, \, All
	loop
		if RegExMatch(path, "i)(?<=%)[\da-f]{1,2}", hex)
			StringReplace, path, path, `%%hex%, % Chr("0x" . hex), All
		else break
	return path
}

Explorer_GetWindow_Kawvin(hwnd="")
{
	WinGet, Process, ProcessName, % "ahk_id" hwnd := hwnd? hwnd:WinExist("A")
	WinGetClass class, ahk_id %hwnd%

	if (Process!="explorer.exe")
		return
	if (class ~= "(Cabinet|Explore)WClass")
	{
		for window in ComObjCreate("Shell.Application").Windows
			if (window.hwnd==hwnd)
				return window
	}
	else if (class ~= "Progman|WorkerW")
		return "desktop" ; desktop found
}