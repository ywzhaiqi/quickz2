﻿/*
Plugin=WordCompare
Name1=Word文件比较[动态]
Command1=DM_WordCompare
Author=Kawvin
Version=0.1
*/

DM_WordCompare(aParam) ; 此Plugin执行的command为Test，代表执行Test函数,函数请预留一个aParam，用于QuickZ传递参数过来。
{
    icon:=A_ScriptDir . "\User\Icons\winword.ico"
    ;~ BCompareMenu := MenuZ_GetSub() ; 获取一个子菜单对象
    BCompareMenu := MenuZ_GetSibling() ; 获取一个同级菜单对象
    BCompareMenu.Add({name:"&W  [Word比较] 左键添加左侧 右键添加右侧并比较 中键上次比较",icon: icon,uid:{Handle:"DM_WordComparing_Handle",Data:aParam}}) ; Handle的值为点击菜单后执行的功能
    return BCompareMenu  ; 必须返回子菜单对象
}


DM_WordComparing_Handle(aMsg, aObj) 
{
    ;Global gMenuZ
    ExtWordSupported=doc;docm;docx;dot;dotm;dotx;htm;html;mht;mhtml;odt;rft;txt;wps;xml
    ExtExcelSupported=csv;dbf;dif;ods;txt;xla;xlam;xls;xlsb;xlsm;xlsx;xlt;xltm;xltx
    FileFullPathList:=QZData("Files")
    CompArray:=[]
    loop,Parse,FileFullPathList,`n,`r
    {
        if(A_index=1)
        {
            SplitPath,A_LoopField,,,MyOutExt
            IfinString, ExtWordSupported, %MyOutExt%
                CompArray[1]:=A_LoopField
        }
        if(A_index=2)
        {
            SplitPath,A_LoopField,,,MyOutExt
            IfinString, ExtWordSupported, %MyOutExt%
                CompArray[2]:=A_LoopField
            break
        }
        CompArray[2]:=""
    }
    If (aMsg = "OnRun")
    {
        MyFun_WordCompareLeft_Handle(CompArray)
    }
	else if(aMsg = "OnRButton")	
    {
		MyFun_WordCompareRight_Handle(CompArray)
    }
    else If (aMsg = "onmbutton")
    {
		MyFun_WordCompareMiddle_Handle()
    }
}

MyFun_WordCompareLeft_Handle(CompArray) 
{
    if(CompArray[2]!="")
        MyFun_WordCompareExecute_Handle(CompArray) 
    else
    {
        SplitPath,A_LineFile,,MyOutDir,,MyOutNameNoExt
        SelectedFile:=CompArray[1]
        IniWrite, %SelectedFile%, %MyOutDir%\%MyOutNameNoExt%.ini, CompareMain, LeftFile
        SelectedFile:=""
        IniWrite, %SelectedFile%, %MyOutDir%\%MyOutNameNoExt%.ini, CompareMain, RightFile
    }
}
MyFun_WordCompareMiddle_Handle()
{
    SplitPath,A_LineFile,,MyOutDir,,MyOutNameNoExt
    IniRead, LeftFile, %MyOutDir%\%MyOutNameNoExt%.ini, CompareMain, LeftFile, %A_Space%
    IniRead, RightFile, %MyOutDir%\%MyOutNameNoExt%.ini, CompareMain, RightFile, %A_Space%
    CompArray:=[]
    CompArray[1]:=LeftFile
    CompArray[2]:=RightFile
    if(LeftFile!="")&&(RightFile!="")
        MyFun_WordCompareExecute_Handle(CompArray) 
    else
        return
    sleep,200
    send,{esc}
}

MyFun_WordCompareRight_Handle(CompArray) 
{
    if(CompArray[2]!="")
        MyFun_WordCompareExecute_Handle(CompArray) 
    else
    {
        SplitPath,A_LineFile,,MyOutDir,,MyOutNameNoExt
        IniRead, LeftFile, %MyOutDir%\%MyOutNameNoExt%.ini, CompareMain, LeftFile, %A_Space%
        SelectedFile:=CompArray[1]
        TemCompArray:=[]
        TemCompArray[1]:=LeftFile
        TemCompArray[2]:=SelectedFile
        IniWrite, %SelectedFile%, %MyOutDir%\%MyOutNameNoExt%.ini, CompareMain, RightFile
        MyFun_WordCompareExecute_Handle(TemCompArray) 
    }
    sleep,200
    send,{esc}
}

MyFun_WordCompareExecute_Handle(CompArray) 
{
    LeftFile:=CompArray[1]
    RightFile:=CompArray[2]
    SetTimer, WordCompareChangeButtonNames, 50 
    MsgBox, 35, Word比较数据确认, 请确认数据：`n【原始文档】：%LeftFile%`n`n【修订文档】：%RightFile%`n`n如果不正确，按【交换】或【取消】
    IfMsgBox,Cancel
        return
    IfMsgBox,no
    {
        TempFile:=LeftFile
        LeftFile:=RightFile
        RightFile:=TempFile
    }
    ;打开程序并加载
    AppPath:=QZData("%WinWord%")
    ;AppPath:="c:\Program Files\Microsoft Office\root\Office16\WINWORD.EXE"
    run,%AppPath%
    WinActivate,%AppPath%
    WinWaitActive ,ahk_class OpusApp,,15
    if ErrorLevel
    {
        MsgBox, 打开Word超时
        return
    }
    sendinput,{alt down}    ;分开写，避免与其他截图类软件冲突
    sleep,50
    sendinput,{alt up}
    sleep,300
    sendinput,r
    sleep,300
    sendinput,m
    sleep,300
    sendinput,c
    sleep,1500
    ;~ ;WinWaitActive ,,比较文档,10
    ;controlclick,MsoCommandBar1,ahk_class bosa_sdm_msword
    sendinput,{tab}
    sleep,300
    sendinput,{enter}
    sleep,1500
    Clipboard=%LeftFile%
    sendinput,^v
    sleep,500
    sendinput,{enter}
    sleep,1000
    ;controlclick,MsoCommandBar2,ahk_class bosa_sdm_msword
    sendinput,{tab 2}
    sleep,300
    sendinput,{enter}
    sleep,1500
    Clipboard=%RightFile%
    sendinput,^v
    sleep,500
    sendinput,{enter}
    ;sleep,1000
    ;sendinput,{tab 4}
    ;sleep,300
    ;sendinput,{enter}
    WordCompareChangeButtonNames: 
        IfWinNotExist, Word比较数据确认
            return  ; Keep waiting.
        SetTimer, WordCompareChangeButtonNames, off 
        WinActivate 
        ControlSetText, Button1, 比较
        ControlSetText, Button2, 交换
        ;ControlSetText, Button2, 取消
    return
}