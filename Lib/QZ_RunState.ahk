﻿

class QZ_RunState_Parser {
	__New(sState) {
		this.parse(sState)
	}

	parse(sState) {
		if (!sState) return

		If InStr(sState, 0)
	        this.state := "Normal"
	    If InStr(sState, 1)
	        this.state := "Max"
	    If InStr(sState, 2)
	        this.state := "Min"
	    If InStr(sState, 3)
	        this.state := "Hide"

	    If InStr(sState, "a")
	        this.Admin := true
	    Else
	        this.Admin := false
	}
} 