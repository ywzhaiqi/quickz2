﻿Return
30A93E7E-2A8C-458D-AA51-AB63CFB94E0D:
;向下移动
Send {down}
Return
32666DED-EC93-4C55-B598-296A848C8A59:
;删除
Send {delete}
Return
41A6D6AF-DFE7-4D86-AF3D-E4AEE3E0A634:
;【示例6】AHK代码模式
msgbox % gMenuZ.Data.WinTitle
Return
4B94C395-7BD8-4253-A832-F0032A2A70C1:
;向左移动
Send {Left}
Return
52783379-56CB-471B-85CC-2ABCE5F9747B:
;切换为普通模式
vimd.changemode("普通模式")
Return
5FBD2A36-9BD7-4413-B1FC-6DE2E3010706:
;向上移动
Send {Up}
Return
6E1B4536-C2C0-40F7-A775-95212189C487:
;切换为VIM模式
vimd.changemode("VIM模式")
Return
75D196AF-EB04-44A4-B513-06A7534E9AE7:
;【示例6】AHK代码模式
msgbox % gMenuZ.Data.WinTitle
Return
9A114C18-B742-49E4-A426-EEA446041B0D:
;回到行前并切换模式
Send {Home}
vimd.changemode("普通模式")
Return
A3BB4CC3-76A5-45C3-811B-B3D029030BD1:
;向右移动
Send {Right}
Return
/*
    功能：QuickZ在加载的时候执行此函数
*/
notepad_Init()
{
    Global VimD
}

/*
    功能：每次切换模式的时候执行此函数
*/
notepad_ChangeMode()
{
    Global VimD
}

/*
    功能：按下热键后，热键对应的功能执行前，会运行此函数
    * 默认返回False，功能正常执行。
    * 如果返回True，功能不会执行。
    可以利用这个函数设置自动判断模式
*/
notepad_ActionBefore()
{
    Global VimD
}

/*
    功能：热键对应的功能执行后，会运行此函数。
    函数不需要返回值
    可以利用这个函数来做状态提醒
*/
notepad_ActionAfter()
{
    Global VimD
}

/*
    功能：热键提醒，用于提示当前有效热键
    QuickZ会传递当前有效热键列表到此函数中。所以需要预留两个参数
    aTemp - 当前热键缓存
    aMore - 符合当前热键缓存的更多热键
*/
notepad_Show(aTemp, aMore)
{
    Global VimD
}

Return
