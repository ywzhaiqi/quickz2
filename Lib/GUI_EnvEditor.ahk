﻿

GUI_EnvEditor_Load(Callback, CloseEvent:="",EnvType:="UserEnv") { 
    Global EnvEditor, gQZConfig
    EnvEditor := new GUI2("EnvEditor", "+Lastfound +Theme -DPIScale")
    cons_fontsize:=strlen(gQZConfig.setting.global.DPIScale)?"s" . gQZConfig.setting.global.DPIScale:QZGlobal.FontSize
    EnvEditor.SetFont(cons_fontsize, "Microsoft YaHei")
    EnvEditor.AddCtrl("Text_Tip", "Text", "x10 y507 w300 h26", QZLang.TextEnvTips)
    EnvEditor.AddCtrl("Text_Tip2", "Text", "x10 y12 h26 hidden", QZLang.TextEnvTips2)
    EnvEditor.AddCtrl("LV_Env", "ListView", "x10 y50 w700 h450 hidden grid altsubmit", QZLang.TextListViewEnv)
    if (EnvType="UserEnv")
    {
        EnvEditor.AddCtrl("CB_UserEnv", "Radio", "x220 y10 w110 h26 hidden checked", QZLang.TextCBUserEnv)
        EnvEditor.AddCtrl("CB_OutsideEnv", "Radio", "x480 y10 w120 h26 hidden", QZLang.TextCBOutsideEnv)
    }
    if (EnvType="OutSideEnv")
    {
        EnvEditor.AddCtrl("CB_UserEnv", "Radio", "x220 y10 w110 h26 hidden", QZLang.TextCBUserEnv)
        EnvEditor.AddCtrl("CB_OutsideEnv", "Radio", "x480 y10 w120 h26 hidden checked", QZLang.TextCBOutsideEnv)
    }
    EnvEditor.AddCtrl("CB_InsideEnv", "Radio", "x350 y10 w120 h26 hidden", QZLang.TextCBInsideEnv)
    EnvEditor.AddCtrl("Button_Manager", "Button", "x625 y10 w85 h26 hidden", QZLang.ButtonManager)
    EnvEditor.AddCtrl("Button_OK", "Button", "x494 y505 w85 h26", QZLang.ButtonOK)
    EnvEditor.AddCtrl("Button_Close", "Button", "x589 y505 w85 h26", QZLang.ButtonClose)
    EnvEditor.AddCtrl("Button_Help", "Button", "x684 y505 h26 w26", "?")
    
    LV_ModifyCol(1, 150)
    LV_ModifyCol(2, 500)
    If IsFunc(Callback)
    {
        EnvEditor.FuncCallback := Callback
        EnvEditor.AddCtrl("Edit_Env", "Edit", "x10 y50 w700 h450")
        EnvEditor.Text_Tip.Hide()
        EnvEditor.Edit_Env.Hide()
        EnvEditor.Button_Help.Hide()
        EnvEditor.Text_Tip2.Show()
        EnvEditor.CB_UserEnv.Show()
        EnvEditor.CB_InsideEnv.Show()
        EnvEditor.CB_OutsideEnv.Show()
        EnvEditor.Button_Manager.Show()
        EnvEditor.LV_Env.Show()
        EnvEditor.LV_Env.OnEvent("GUI_EnvEditor_Event")
        EnvEditor.Button_OK.OnEvent(Callback)
        EnvEditor.Button_Manager.OnEvent("GUI_EnvEditor_Manager")
        EnvEditor.CB_UserEnv.OnEvent("GUI_EnvEditor_UserEnvLoad")
        EnvEditor.CB_InsideEnv.OnEvent("GUI_EnvEditor_InsideEnvLoad")
        EnvEditor.CB_OutsideEnv.OnEvent("GUI_EnvEditor_OutsideEnvLoad")
        ;~ GUI_EnvEditor_UserEnvLoad()
        ;以下自用修改
        if (EnvType="OutSideEnv")
            GUI_EnvEditor_OutsideEnvLoad()
        else
            GUI_EnvEditor_UserEnvLoad()
    }
    Else
    {
        EnvEditor.AddCtrl("Edit_Env", "Edit", "x10 y10 w700 h485")
        ;~ EnvEditor.SetFont(QZGlobal.FontSize, "Microsoft YaHei")
        EnvEditor.AddCtrl("Button_AutoAdd", "Button", "x304 y505 w85 h26", QZLang.ExeAutoAdd)    ;Kawvin修改
        EnvEditor.AddCtrl("Button_ManualAdd", "Button", "x399 y505 w85 h26", QZLang.ExeManualAdd)    ;Kawvin修改
        GUI_EnvEditor_EditLoad()
        EnvEditor.Button_AutoAdd.OnEvent("GUI_EnvEditor_ExeAutoAdd")      ;Kawvin修改
        EnvEditor.Button_ManualAdd.OnEvent("GUI_EnvEditor_ExeManualAdd")      ;Kawvin修改
        EnvEditor.Button_OK.OnEvent("GUI_EnvEditor_UpdateOK")
        
    }
    EnvEditor.Button_Close.OnEvent("GUI_EnvEditor_Destroy")
    EnvEditor.Button_Help.OnEvent("GUI_EnvEditor_Help")
    if(EnvType="UserEnv")
        EnvEditor.Show("", QZLang.TitleIUserEnv)
    if(EnvType="OutSideEnv")
        EnvEditor.Show("", QZLang.TitleIOutsideEnv)
}
;Kawvin修改--------------------------------------
GUI_EnvEditor_ExeAutoAdd() { 
    MsgBox, 36, 自动添加提示, 【重要提示】自动添加可能会出错，请先备份已有变量文本`n------------------------------------------`n说明：`n1、变量名称是简写的可能会出错，例如【ps.exe】可能会匹配【Desktops.exe】；`n2、变量名称后缀会自动添加【.exe】，例如【ps】会搜索【ps.exe】；`n3、变量名称中的【空格】后自动变换为【_】；`n4、变量名称中如果无【=】会跳过，如【winrar=】会搜索，【winrar】则跳过；`n5、第一个字符为【`;】的，默认为注释行，跳过；`n------------------------------------------`n`n如果变量较多，会搜索时间较长，请耐心等待`n`n确认进行自动添加？
    ifmsgbox ,No
        return
    ;~ Prompt=请输入自动查找模式：`n`n【模式1】：全自动查找，如果出现多项内容，则默认第1项；`n`n【模式2】：选择式查找，如果出现多项内容，则跳出对话框，要求在前10项选择查找到的内容项；`n`n`n默认使用【模式1】
    ;~ InputBox, SearchMode , 查找模式选择, %Prompt%, ,680 , 320, , , , 20, 1
    ;~ if ErrorLevel
        ;~ return
    ;~ if (SearchMode!="2")
        ;~ SearchMode:=1
    SetTimer, EnvEditorChangeButtonNames, 50 
    MsgBox, 3, 变量查找模式选择, 请选择自动查找模式：`n`n【全自动】：全自动查找，如果出现多项内容，则默认第1项；`n【选择式】：选择式查找，如果出现多项内容，则要求在前10项选择内容项；
    ifmsgbox,Cancel
        return
    ifmsgbox,Yes
        SearchMode:=1
    ifmsgbox,No
        SearchMode:=2
    objGUI :=GUI_EnvEditor_Dump()
    OldText := objGUI.Edit_Env.GetText()
    NewEVString:=EveryExePath(OldText,SearchMode)
    ;~ MsgBox % NewEVString
    NewText:=NewEVString
    objGUI.Edit_Env.SetText(NewText)
    ;~ x:=1
    ;~ while x=1
    ;~ {
        ;~ process,close,Everything.exe
        ;~ msgbox , % ErrorLevel
        ;~ if ErrorLevel=0
            ;~ break
    ;~ }
    ;~ while x=1
    ;~ {
        ;~ process,close,Everything_x64.exe
        ;~ msgbox , % ErrorLevel
        ;~ if ErrorLevel=0
            ;~ break
    ;~ }
    MsgBox, 0, 完成提示, 自动添加已完成, 3
    EnvEditorChangeButtonNames: 
        IfWinNotExist, 变量查找模式选择
            return  ; Keep waiting.
        SetTimer, EnvEditorChangeButtonNames, off 
        WinActivate 
        ControlSetText, Button1, 全自动
        ControlSetText, Button2, 选择式
        ;ControlSetText, Button2, 取消
    return
}
GUI_EnvEditor_Help() { 
    _hwnd := EnvEditor.hwnd
    MouseGetPos,CurX,CurY,,,,  ahk_id %_hwnd%
    PosX := GuiX  + A_GuiWidth
    PosY := GuiY  + A_GuiHeight
    objInfo := QZLang.Help_EvnEditor()
    objTT := TT("CloseButton", "", "")
    ObjTT.Show(objInfo.Text , PosX, PosY, objInfo.Title)
}
GUI_EnvEditor_ExeManualAdd() { 
    FileSelectFile, SelectedExeFile, M3, ,选择程序文件（可以多选）, 程序文件(*.exe)
    if ErrorLevel
        return
    if SelectedExeFile =
        return
    else
    {
        NewEVString:=
        NewExePath:=
        Loop, parse, SelectedExeFile, `n
        {
            if a_index = 1
            {
                NewExePath:=A_LoopField
                if (substr(NewExePaht,0)!="\")
                    NewExePath=%NewExePath%\
            } else {
                TemEV:=A_LoopField
                ;~ StringLower, TemEV, TemEV
                ;~ TemEV:=StrReplace(TemEV,".exe","")
                TemEV:=SubStr(TemEV,1,strlen(TemEV)-4)
                TemEV:=StrReplace(TemEV," ","_")
                TemEV:=StrReplace(TemEV,"　","_")
                if a_index = 2
                    NewEVString=%TemEV%`=%NewExePath%%A_LoopField%
                else
                    NewEVString=%NewEVString%`n%TemEV%`=%NewExePath%%A_LoopField%
            }
        }
        objGUI :=GUI_EnvEditor_Dump()
        OldText := objGUI.Edit_Env.GetText()
        NewText=%OldText%`n%NewEVString%
        objGUI.Edit_Env.SetText(NewText)
        ;~ objGUI.Edit_Env.Append(NewEVString)
    }
}
;Kawvin修改--------------------------------------

GUI_EnvEditor_Save() { 
    Global EnvEditor
    Return EnvEditor.Select
}

GUI_EnvEditor_Dump() { 
    Global EnvEditor
    Return EnvEditor
}

GUI_EnvEditor_Destroy() { 
    Global EnvEditor
    EnvEditor.Destroy()
}

GUI_EnvEditor_Event() { 
    Global EnvEditor
    If (A_GuiEvent = "Normal")
    {
        LV_GetText(strSelect, A_EventInfo)
        EnvEditor.Select := strSelect
    }
    If (A_GuiEvent = "DoubleClick")
    {
        LV_GetText(strSelect, A_EventInfo)
        EnvEditor.Select := strSelect
        _Func := EnvEditor.FuncCallback
        %_Func%()
    }
}

GUI_EnvEditor_UpdateOK() { 
    GUI_EnvEditor_Update()
    GUI_EnvEditor_Destroy()
}

GUI_EnvEditor_Update() { 
    Global EnvEditor, gQZConfig
    EnvList := EnvEditor.Edit_Env.GetText()
    If EnvEditor.CB_UserEnv.GetText()
    {
        arrEnv := []
        idx := 1
        Loop, Parse, EnvList, `n
        {
            If !Strlen(A_LoopField)
                Continue
            arrEnv[idx] := {Name:Regexreplace(A_loopfield, "=.*$"), Value:Regexreplace(A_loopfield, "^[^=]*=")}
            idx++
        }
        gQZConfig.Setting.UserEnv := arrEnv
        QZ_UpdateUserEnv()
        GUI_EnvEditor_UserEnvLoad()
    }
    If EnvEditor.CB_OutsideEnv.GetText()
    {
        FileDelete, % QZGlobal.OutsideEnv
        BackFileEncoding := A_FileEncoding
        FileEncoding, UTF-16
        FileAppend, %EnvList%, % QZGlobal.OutsideEnv
        FileEncoding, %BackFileEncoding%
        GUI_EnvEditor_OutsideEnvLoad()
    }
    GUI_Save()
}



GUI_EnvEditor_EditLoad() { 
    Global EnvEditor, gQZConfig
    EnvList := ""
    If EnvEditor.CB_UserEnv.GetText()
    {
        Loop % gQZConfig.Setting.UserEnv.MaxIndex()
        {
            objEnv := gQZConfig.Setting.UserEnv[A_Index]
            EnvList .= objEnv.Name "=" objEnv.Value
            If IsObject(gQZConfig.Setting.UserEnv[A_Index+1])
                EnvList .= "`n"
        }
    }
    If EnvEditor.CB_OutsideEnv.GetText()
    {
        If FileExist(QZGlobal.OutsideEnv)
            FileRead, EnvList, % QZGlobal.OutsideEnv
    }
    EnvEditor.Edit_Env.SetText(EnvList)
}

GUI_EnvEditor_UserEnvLoad() { 
    Global EnvEditor, gQZConfig
    EnvEditor.Default()
    LV_Delete()
    Loop % gQZConfig.Setting.UserEnv.MaxIndex()
    {
        objEnv := gQZConfig.Setting.UserEnv[A_Index]
        LV_Add("","%" objEnv.Name "%", objEnv.Value)
    }
}

GUI_EnvEditor_InsideEnvLoad() { 
    Global EnvEditor, gQZConfig
    EnvEditor.Default()
    LV_Delete()
    varList := "Apps|User|Icons|" QZGlobal.InsideEnv "|" QZGlobal.SystemEnv
    Loop, Parse, varList, |
        LV_Add("", "%" A_LoopField "%", QZ_ReplaceEnv( "%" A_LoopField "%" ) )
}

GUI_EnvEditor_OutsideEnvLoad() { 
    Global EnvEditor, gQZConfig
    EnvEditor.Default()
    LV_Delete()
    FileRead, EnvText, % QZGlobal.OutsideEnv
    Loop, Parse, EnvText, `n, `r
    {
        If Strlen(A_LoopField)
        {
            If RegExMatch(A_LoopField, "^\s*\[.*\]\s*$")
                Continue
            If InStr(A_LoopField, "=")
                LV_Add("", "%" Regexreplace(A_loopfield, "=.*$") "%", Regexreplace(A_loopfield, "^[^=]*="))
            Else
                LV_Add("", A_LoopField, "")
        }
    }
}

GUI_EnvEditor_Manager() { 
    Global EnvEditor
    If EnvEditor.CB_InsideEnv.GetText()
        Return
    EnvEditor.Button_Manager.SetText(QZLang.ButtonSave)
    EnvEditor.Button_Manager.OnEvent("GUI_EnvEditor_ManagerOK")
    EnvEditor.CB_UserEnv.Disable()
    EnvEditor.CB_InsideEnv.Disable()
    EnvEditor.CB_OutsideEnv.Disable()
    EnvEditor.Edit_Env.Show()
    EnvEditor.Text_Tip.Show()
    EnvEditor.LV_Env.Hide()
    EnvEditor.Button_OK.Hide()
    EnvEditor.Button_Close.Hide()
    GUI_EnvEditor_EditLoad()
}

GUI_EnvEditor_ManagerOK() { 
    Global EnvEditor
    EnvEditor.Button_Manager.SetText(QZLang.ButtonManager)
    EnvEditor.Button_Manager.OnEvent("GUI_EnvEditor_Manager")
    EnvEditor.CB_UserEnv.Enable()
    ;EnvEditor.CB_UserEnv.SetText(True)
    EnvEditor.CB_InsideEnv.Enable()
    EnvEditor.CB_OutsideEnv.Enable()
    EnvEditor.Edit_Env.Hide()
    EnvEditor.Text_Tip.Hide()
    EnvEditor.LV_Env.Show()
    EnvEditor.Button_OK.Show()
    EnvEditor.Button_Close.Show()
    GUI_EnvEditor_Update()
}
