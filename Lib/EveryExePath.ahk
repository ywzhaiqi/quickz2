﻿
EveryExePath(str,SearchMode){
	global QZGlobal
	;~ if !EverythingExist()
		;~ return
	gosub,EverythingExist
	if !EverythingRun()
		return	
	ReturnString:=
	Loop, parse, str, `n,`r
	{
		If (substr(A_loopfield,1,1)=";")	 or (substr(A_loopfield,1,1)="；")				;本行第1个字符为；注释，过
		{
			ReturnString .= A_loopfield "`r`n"		;本行没有等号，过
			continue
		}
        IfNotInString,A_loopfield,=
		{
			ReturnString .= A_loopfield "`r`n"		;本行没有等号，过
			continue
		}
		ItemKey:=Trim(RegExReplace(A_LoopField,"\s*=.*?$") )        ;本条目的 等号 左侧部分
		ItemVal:=Trim(RegExReplace(A_LoopField,"^.*?=")  )          ;本条目的 等号 右侧部分
		TemEV:=StrReplace(ItemKey,"_"," ")
		TemExt:=substr(TemEV,-3)
		stringlower,TemExt,TemExt
		if (TemExt!=".exe")
			TemEV.= ".exe"
		;~ MsgBox % TemEV
		TemVal:=EveryPath(TemEV,SearchMode)
		ReturnString .= ItemKey "=" TemVal "`r`n"
	}
	return ReturnString
}

;~;[使用everything查找任何文件程序的路径]
EveryPath(str,SearchMode){
	ev := new everything
	;~ str := "file:*.exe|*.lnk !C:\*Windows*"		;[使用everything查找任何文件程序的路径]
	UserEvn:=str
	str.=" !C:\*Windows*"								;[使用everything搜索所有exe程序]
	ev.SetSearch(str)
	ev.Query()			;执行搜索
	sleep 100
	if (SearchMode="2")
	{
		Prompt=变量名称：%UserEvn%`n`n请输入要使用的路径编号：【0`-9】，20秒内未选择，则默认第【0】项`n
		if ev.GetTotResults()>1
		{
			loop,% ev.GetTotResults()
			{
				FindID:=A_index-1
				FindStr:=ev.GetResultFullPathName(FindID)
				Prompt=%Prompt%`【%FindID% `】`  %FindStr%`n
				if a_index=10
					break
			}
			InputBox, SearchMode , 路径选择, %Prompt%, ,680 ,380 , , , , 20, 0
			if ErrorLevel
				return
			if SearchMode not in {0,1,2,3,4,5,6,7,8,9}
				SearchMode=0
			return ev.GetResultFullPathName(SearchMode)
		} else {
			return ev.GetResultFullPathName(0)
		}
	} else
	return ev.GetResultFullPathName(0)
	;~ Loop,% ev.GetTotResults()
	;~ {
		;~ Z_Index:=A_Index-1
		;~ MenuObj[(RegExReplace(ev.GetResultFileName(Z_Index),"iS)\.(exe|lnk)$",""))]:=ev.GetResultFullPathName(Z_Index)
	;~ }
}

;检查Everything是否运行，运行返回：True
EverythingRun(){
	global QZGlobal
	EvIsRun:=True
	while !WinExist("ahk_exe Everything.exe")
	{
		Sleep,100
		EVPath:=QZGlobal.EvPath
		IfExist,%EVPath%
		{
			Run,%EVPath% -startup
			Sleep,1000
			break
		} else {
			MsgBox,16,,需要Everything快速识别程序的路径`n请设置正确路径或下载Everything：http://www.voidtools.com/
			EvIsRun:=false
			break
		}
	}
	return EvIsRun
}

;检查Everything是否存在，运行返回：True
EverythingExist:
	global everyDLL:="Everything.dll"
	if(FileExist(A_ScriptDir "\Lib\Everything.dll")){
		everyDLL:=DllCall("LoadLibrary", str, A_ScriptDir "\Lib\Everything.dll") ? "Everything.dll" : "Everything64.dll"
	}
	if(FileExist(A_ScriptDir "\Lib\Everything64.dll")){
		everyDLL:=DllCall("LoadLibrary", str, A_ScriptDir "\Lib\Everything64.dll") ? "Everything64.dll" : "Everything.dll"
	}
	IfNotExist,%A_ScriptDir%\Lib\%everyDLL%
		MsgBox,16,,没有找到%everyDLL%，将不能识别菜单中程序的路径`n请复制%everyDLL%到%A_ScriptDir%\Lib\目录下
	else
		everyDLL=%A_ScriptDir%\Lib\%everyDLL%
return 

class everything
{
	__New(){
		;~ msgbox % everyDLL
		this.hModule := DllCall("LoadLibrary", str, everyDLL)
	}
	__Get(aName){
	}
	__Set(aName, aValue){
	}
	__Delete(){
		DllCall("FreeLibrary", "UInt", this.hModule) 
		return
	}
	SetSearch(aValue)
	{
		this.eSearch := aValue
		dllcall(everyDLL "\Everything_SetSearch",str,aValue)
		return
	}
	Query(aValue=1)
	{
		dllcall(everyDLL "\Everything_Query",int,aValue)
		return
	}
	GetTotResults()
	{
		return dllcall(everyDLL "\Everything_GetTotResults")
	}
	GetResultFileName(aValue)
	{
		return strget(dllcall(everyDLL "\Everything_GetResultFileName",int,aValue))
	}
	GetResultFullPathName(aValue,cValue=128)
	{
		VarSetCapacity(bValue,cValue*2)
		dllcall(everyDLL "\Everything_GetResultFullPathName",int,aValue,str,bValue,int,cValue)
		return bValue
	}
}