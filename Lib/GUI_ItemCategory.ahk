﻿#Include %A_ScriptDir%\Lib\QZ_UpdategQZConfig.ahk

GUI_ItemCategory_Load(Callback, CloseEvent, aCategory) { 
    Global ItemCategory, gQZConfig
    ItemCategory := new GUI2("ItemCategory", "+Lastfound +Theme -DPIScale")
    ;~ ItemCategory.SetFont(QZGlobal.FontSize, "Microsoft YaHei")
    cons_fontsize:=strlen(gQZConfig.setting.global.DPIScale)?"s" . gQZConfig.setting.global.DPIScale:QZGlobal.FontSize
    ItemCategory.SetFont(cons_fontsize, "Microsoft YaHei")
    ItemCategory.AddCtrl("Text_Name", "Text",  "x10 y12 h28", QZLang.TextCategoryName)
    ItemCategory.AddCtrl("Edit_Name", "Edit", "x90 y10 w180 h24 readonly", aCategory)
    ItemCategory.AddCtrl("TextTips", "Text",  "x10 y42 h28 w100", QZLang.TextSelectCategory)
    ItemCategory.AddCtrl("TextTips2", "Text",  "x250 y42 h28 w120", " 右键：管理类别")
    ItemCategory.AddCtrl("LV_Category", "ListView", "x10 y70 w360 r15 AltSubmit -hdr +grid","Title")
    ItemCategory.AddCtrl("Edit_Category", "Edit", "x10 y70 w340 r18")
    ;~ ItemCategory.AddCtrl("ButtonManager", "Button", "x275 y38 w100 h26", QZLang.ButtonManager)
    ItemCategory.AddCtrl("ButtonClear", "Button", "x275 y10 w100 h26", QZLang.ButtonClear2)
    ItemCategory.Edit_Category.Hide()
    ItemCategory.LV_Category.OnEvent("GUI_ItemCategory_Event")
    ;~ ItemCategory.ButtonManager.OnEvent("GUI_ItemCategory_Manager")
    ItemCategory.ButtonClear.OnEvent("GUI_ItemCategory_Clear")
    ItemCategory.Show("","类别管理")
    ItemCategory.Callback := Callback
    If Strlen(CloseEvent)
        ItemCategory.OnClose(CloseEvent)
    CategoryList := gQZConfig.setting.global.CategoryList
    LV_Delete()
    Loop, Parse, CategoryList, `n
        If strlen(A_LoopField)
            LV_Add("", A_LoopField)
    Return ItemCategory
}


GUI_ItemCategory_Dump() { 
    Global ItemCategory
    Return ItemCategory
}

GUI_ItemCategory_Destroy() { 
    Global ItemCategory
    ItemCategory.Destroy()
}

GUI_ItemCategory_Event() { 
    Global ItemCategory,gQZConfig
    If (A_GUIEvent  = "DoubleClick") && A_EventInfo
    {
        LV_GetText(strSelect, A_EventInfo)
        ItemCategory.Select := strSelect
        _Func := ItemCategory.Callback
        %_Func%()
        ;msgbox % A_EventInfo
    }
   if (A_GUIEvent = "RightClick" ) 
    {
        strSelect:=""
        LV_GetText(strSelect, A_EventInfo)
        ;~ msgbox % strSelect
        _hwnd := ItemCategory.hwnd
        MouseGetPos,CurX,CurY,,,,  ahk_id %_hwnd%
        Menu, RMenuCategoryEdit, Add
        Menu, RMenuCategoryEdit, DeleteAll
        Menu, RMenuCategoryEdit, Add,新建类别, _MenuHandle_CategoryEdit
        Menu, RMenuCategoryEdit, Add,修改类别, _MenuHandle_CategoryEdit
        if (strSelect="Title")
            Menu, RMenuCategoryEdit,Disable,修改类别
        Menu, RMenuCategoryEdit, Add,删除类别, _MenuHandle_CategoryEdit
        if (strSelect="Title")
            Menu, RMenuCategoryEdit,Disable,删除类别
        Menu, RMenuCategoryEdit, Show, %CurX%, %CurY%
        Return
        _MenuHandle_CategoryEdit:
        if(A_ThisMenuItem="新建类别")
        {
            ;提示输入信息并检验是否已存在
            InputBox, NewCategory, 输入, 请输入分类名称, , 200,120
            if ErrorLevel
                return
            if (NewCategory="")
                return
            CategoryList := gQZConfig.setting.global.CategoryList
            Loop, Parse, CategoryList, `n
            {
                If strlen(A_LoopField)
                {
                    if(NewCategory=A_LoopField)
                    {
                        MsgBox, 48, 提示, 当前分类名称：%NewCategory%`n`n已存在，请重新建立
                        return
                    }
                }
            }
            ;保存数据并刷新窗体
            CategoryList.="`n" NewCategory
            gQZConfig.Setting.Global.CategoryList:=CategoryList
            ItemCategory.Default()
            LV_Delete()
            Loop, Parse, CategoryList, `n
                If strlen(A_LoopField)
                    LV_Add("", A_LoopField)
        } 
        else if(A_ThisMenuItem="修改类别")
        {
            ;提示输入信息并检验是否已存在
            InputBox, NewCategory, 输入, 请输入分类名称, , 200,120
            if ErrorLevel
                return
            if (NewCategory="")
                return
            CategoryList := gQZConfig.setting.global.CategoryList
            Loop, Parse, CategoryList, `n
            {
                If strlen(A_LoopField)
                {
                    if(NewCategory=A_LoopField)
                    {
                        MsgBox, 48, 提示, 当前分类名称：%NewCategory%`n`n已存在，请重新建立
                        return
                    }
                }
            }
            ;再次确认数据修改
            MsgBox, 4, 提示, 请确认以下数据：`n`n【原类别名称】：%strSelect%`n【现类别名称】：%NewCategory%
            IfMsgBox,No
                return
            ;更新所有数据内的分类
            ifMsgbox,Yes
                QZ_UpdateCategory(strSelect,NewCategory)
            ;保存数据并刷新窗体
            CategoryList:=StrReplace(CategoryList,strSelect,NewCategory)
            gQZConfig.Setting.Global.CategoryList:=CategoryList
            ItemCategory.Default()
            LV_Delete()
            Loop, Parse, CategoryList, `n
                If strlen(A_LoopField)
                    LV_Add("", A_LoopField)
        } 
        else if(A_ThisMenuItem="删除类别")
        {
            ;再次确认数据修改
            MsgBox, 4, 提示, 请确认删除以下类别：`n`n【类别名称】：%strSelect%
            IfMsgBox,No
                return
            ;更新所有数据内的分类
            CategoryList := gQZConfig.setting.global.CategoryList
            ifMsgbox,Yes
                QZ_UpdateCategory(strSelect,"")
            ;保存数据并刷新窗体
            CategoryList:=StrReplace(CategoryList,strSelect,"")
            CategoryList:=StrReplace(CategoryList,"`n`n","`n")
            gQZConfig.Setting.Global.CategoryList:=CategoryList
            ItemCategory.Default()
            LV_Delete()
            Loop, Parse, CategoryList, `n
                If strlen(A_LoopField)
                    LV_Add("", A_LoopField)
        } 
        Return
    }
}


GUI_ItemCategory_Clear() { 
    Global ItemCategory
    ItemCategory.Edit_Name.SetText("")
    ItemCategory.Select := ""
     _Func := ItemCategory.Callback
        %_Func%()
}

GUI_ItemCategory_Manager() { 
    Global ItemCategory, gQZConfig
    ItemCategory.LV_Category.Hide()
    ItemCategory.Edit_Category.Show()
    ItemCategory.Edit_Category.SetText(gQZConfig.Setting.Global.CategoryList)
    ItemCategory.ButtonManager.SetText(QZLang.ButtonOK)
    ItemCategory.ButtonManager.OnEvent("GUI_ItemCategory_ManagerOK")
}


GUI_ItemCategory_ManagerOK() { 
    Global ItemCategory, gQZConfig
    ItemCategory.Default()
    ItemCategory.LV_Category.Show()
    ItemCategory.Edit_Category.Hide()
    ItemCategory.ButtonManager.SetText(QZLang.ButtonManager)
    ItemCategory.ButtonManager.OnEvent("GUI_ItemCategory_Manager")
    CategoryList := ItemCategory.Edit_Category.GetText()
    gQZConfig.setting.global.CategoryList := CategoryList 
    LV_Delete()
    Loop, Parse, CategoryList, `n
        If strlen(A_LoopField)
            LV_Add("", A_LoopField)
    GUI_Save()
}

