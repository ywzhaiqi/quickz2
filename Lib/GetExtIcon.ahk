﻿; 根据ext后缀名获取对应人 iconfile 和 iconnumber，用于菜单生成
GetExtIcon(ext,ByRef IconFile,ByRef IconNumber){

    If RegExMatch(ext,"i)^MultiFiles$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 54
        Return
    }
    else If RegExMatch(ext,"i)^Folder$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 4
        Return
    }

    else If RegExMatch(ext,"i)^Drive$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 9
        Return
    }

    else If RegExMatch(ext,"i)^\.lnk$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 264
        Return
    }

    else If RegExMatch(ext,"i)^\.qza$") {
        ;~ IconFile    := A_ScriptDir "\ICONS\MenuZ.icl"
        IconFile    := A_ScriptDir "\User\ICONS\MenuZ.icl"      ;Kawvin修改
        IconNumber  := 0
        Return
    }

    else If RegExMatch(ext,"i)^text$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 267
        Return
    }

    else If RegExMatch(ext,"i)^NoExt$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 291
        Return
    }


    else If RegExMatch(ext,"i)^.exe$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 2
        Return
    }

    else If RegExMatch(ext,"i)^[^\.]*$") {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 268
        Return
    }

    RegRead,file,HKEY_CLASSES_ROOT,%ext%
    If Strlen(file) = 0 {
        IconFile    := A_WinDir "\system32\shell32.dll"
        IconNumber  := 290
        Return
    }
    RegRead,IconString,HKEY_CLASSES_ROOT,%file%\DefaultIcon
    If ErrorLevel {
        IconFile := A_Windir "\system32\shell32.dll"
        IconNumber := 291
    }
    else 
    {
         IconFile   := RegExReplace(IconString,",[-\s\d]*","")
         IconNumber := RegExReplace(IconString,".*,","")
    }
    If RegExMatch(IconString,"%1") or Not IconString
    {
        RegRead,IconPath,HKCR,%file%\Shell\Open\Command
        if not IconPath
          RegRead,IconPath,HKCR,%file%\Shell\Edit\Command
        RegExMatch(iconpath, "i).:\\.*\.exe", iconfile)
        iconnumber := 0
    }
    Else
    {
        IconFile   := RegExReplace(IconString,",-?\d*","")
        IconNumber := RegExReplace(IconString,".*,","")
    }
}
