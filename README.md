# QuickZ v2

在 kawvin 版本上修改如下：

1. 运行前自动激活原窗口。这样 `send, ^v` 之类直接有效

## 使用说明

**注意：修改后需按 F4 保存并重启QZ**

### 热键在哪修改？

在 `全局设置` 中修改。具体热键见 [ahk 帮助文档](https://www.autohotkey.com/docs/KeyList.htm)